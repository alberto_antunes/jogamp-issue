package com.test.window;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.Animator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static GLWindow window;
    public static Animator animator;
    
    public static ExecutorService pool;
    
    public static GLProfile glp;
    
    public static GLCapabilities caps;
    
    public static void main( String[] args )
    {
        // Running without thread throws RuntimeException
        // with error message: Size 40000x40000 exceeds on of the maxima renderbuffer size 16384
        // Thats okay, just need to catch the exception
//        try {
//            WithoutThread();
//        } catch(RuntimeException e) {
//            System.out.println(e.getCause());
//            window = null;
//        }
        
        WithThread();
    }
    
    public static void WithoutThread() {
        glp = GLProfile.getDefault();
        caps = new GLCapabilities(glp);
        caps.setBackgroundOpaque(false);
        caps.setOnscreen(false);

        window = GLWindow.create(caps);
        animator = new Animator(window);

        window.setSize(40000, 40000);
        window.setVisible(true);
    }
    
    public static void WithThread() {
        pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
        pool.execute(new WorkerThread());
    }
}
