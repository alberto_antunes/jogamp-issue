package com.test.window;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.Animator;

public class WorkerThread implements Runnable {

    public void run() {
        App.glp = GLProfile.getDefault();
        App.caps = new GLCapabilities(App.glp);
        App.caps.setBackgroundOpaque(false);
        App.caps.setOnscreen(false);

        App.window = GLWindow.create(App.caps);
        App.animator = new Animator(App.window);

        App.window.setSize(40000, 40000);
        
        //Just catching exception is not enough, thread stays alive stuck in infinite loop
        //Only exits the loop if i call window.destroy
        //The infinite loop can be observed by placing a breakpoint into the jogamp.newt.DefaultEDTUtil class
        //at line 347 dispatchMessages.run();
        try {
            App.window.setVisible(true); 
        } catch(RuntimeException e) {
            System.out.println(e.getCause());
            //App.window.destroy();
        }        
    }

}
